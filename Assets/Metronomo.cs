﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Metronomo : MonoBehaviour
{
    float timer = 0;
    float time = 0;

    [SerializeField] AudioSource AudioSource;
    // Start is called before the first frame update
    void Start()
    {
        AudioSource = GetComponent<AudioSource>();
        time = Random.Range(6f, 40f);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > time)
        {
            AudioSource.Play();
            time = Random.Range(6f, 40f);
            timer = 0;
        }
    }
}
