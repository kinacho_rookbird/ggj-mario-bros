﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAppears : MonoBehaviour
{
    [SerializeField] Enemy Enemy;
    [SerializeField] PlayerCamera PlayerCamera;
    [SerializeField] SoundData ScarySound;

    [SerializeField] float timeBetweenScary = 2;

    // Start is called before the first frame update
    void Start()
    {
        if (!Enemy)
            Enemy = FindObjectOfType<Enemy>();
        if (!PlayerCamera)
            PlayerCamera = GetComponent<PlayerCamera>();
    }


    float timer = 0;

    bool scary = false;

    private void Update()
    {
        if (PlayerCamera.LookToPosition(Enemy.transform))
        {
            
            if (!scary)
            {
                timer = 0;
                SoundManager.Instance.PlayCutSound(ScarySound.GetRandom(), transform);
                if (!Character.Instance.Invulnerable)
                {
                    if (!GameManager.Instance.enemyEnabled)
                    {
                        Enemy.WakeUp();
                        GameManager.Instance.enemyEnabled = true;
                    }
                    MusicManager.Instance.PlayEnemyMusic();


                }
            }
            scary = true;
        }
        else
        {
            timer += Time.deltaTime / timeBetweenScary;
            if (timer > 1)
            {
                scary = false;
            }
        }
    }
}
