﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stamina : MonoBehaviour
{

    public float value = 1;
    [SerializeField] float StaminaReduction = 0.1f;

    [SerializeField] AudioSource AudioSource;
    [SerializeField] SoundData ExhaustedSound;
    bool running = false;
    bool tired = false;
    private void Update()
    {
        if (Character.Instance.dead) return;
        if (running)
        {
            value -= Time.deltaTime * StaminaReduction;
            //no debe bajar de 0
            if (value < 0) 
            {
                tired = true;
                value = 0; 
            }

            if(value <= 0 && !AudioSource.isPlaying)
            {
                AudioSource.clip = ExhaustedSound.GetRandom();
                AudioSource.Play();
            }
        }
        else
        {
            value += Time.deltaTime * StaminaReduction*0.5f;

            if (value > 1)
            {
                value = 1;
                tired = false;
            }
        }
    }

    internal bool CanRun()
    {
        return !tired;

    }

    internal void Walk()
    {
        running = false;

    }

    public void RunFast()
    {
        running = true;

    }
}
