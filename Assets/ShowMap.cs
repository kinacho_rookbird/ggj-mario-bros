﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMap : MonoBehaviour
{
    [SerializeField]GameObject Map;
    // Start is called before the first frame update
    void Start()
    {
        Map.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Map.SetActive(true);
            Time.timeScale = 0;
        }

        if (Input.GetKeyUp(KeyCode.Tab))
        {
            Map.SetActive(false);
            Time.timeScale = 1;
        }
    }


    public void Close()
    {

    }

}
