﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Charactermove : MonoBehaviour
{

    [SerializeField] Stamina Stamina;

    private CharacterController controllerP;
    public float runSpeed;
    public float walkSpeed;
    private float currentSpeed;

    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        controllerP = GetComponent<CharacterController>();
    }

    private void OnDisable()
    {
        anim.SetBool("runFast", false); 
        anim.SetBool("run", false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && Stamina.CanRun())
        {
            Stamina.RunFast();
            currentSpeed = runSpeed;
            anim.SetBool("runFast", true);
        }

        else /*(Input.GetKeyUp(KeyCode.LeftShift)*/
        {
            Stamina.Walk();
            currentSpeed = walkSpeed;
            anim.SetBool("runFast", false);
        }


        Vector3 direction2 = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        direction2 = direction2.normalized * currentSpeed * Time.deltaTime;
        controllerP.Move(direction2);

        if (controllerP.velocity.magnitude >= 0.1f)
            anim.SetBool("run", true);
        else if (controllerP.velocity.magnitude == 0)
            anim.SetBool("run", false);


        if ((Input.GetAxis("Horizontal") != 0 || (Input.GetAxis("Vertical") != 0)))
        {
            float angularSpeed = 360 * 2;
            float beta = Quaternion.Angle(transform.rotation, Quaternion.LookRotation(direction2));

            float T = beta / angularSpeed;

            transform.rotation = 
                Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction2), Time.deltaTime/T);
        }
        controllerP.Move(new Vector3(0, -3f * Time.deltaTime, 0));

    }
}
