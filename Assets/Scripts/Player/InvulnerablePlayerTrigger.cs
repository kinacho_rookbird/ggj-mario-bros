﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvulnerablePlayerTrigger : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        var character = other.GetComponent<Character>();
        if (character)
        {
            character.Invulnerable = true;
            if (GameManager.Instance.enemyEnabled)
                MusicManager.Instance.PlayEnviromentMusic();
        }

    }

    private void OnTriggerExit(Collider other)
    {
        var character = other.GetComponent<Character>();
        if (character)
        {
            character.Invulnerable = false;
        }
    }

    private void OnDestroy()
    {
        Character.Instance.Invulnerable = false;
        
    }
}
