﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSteps : MonoBehaviour
{
    [SerializeField] SoundData FootSteps;
    [SerializeField] bool sequenced = false;

    int index =0;

    public void FootStep()
    {
        if (!sequenced)
            SoundManager.Instance.PlayEffect(FootSteps.GetRandom(), transform);
        else
        {
            SoundManager.Instance.PlayEffect(FootSteps.Clips[index], transform);
            index = (index + 1) % FootSteps.Clips.Length;
        }
    }

    public void ResetSound()
    {
        index = 0;
    }

    public void EndDead()
    {
        Character.Instance.Restart();
    }

}
