﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Character : MonoBehaviour
{
    public static Character Instance;
    [SerializeField]Charactermove Movement;

    Vector3 InitPosition;
    Quaternion InitRotation;

    InteracibleObject CurrentObject;

    [SerializeField] SoundData DeathSound;

    private void Awake()
    {
        Instance = this;
        Movement = GetComponent<Charactermove>();
        InitPosition = transform.position;
        InitRotation = transform.rotation;
    }

    public void SetObject(InteracibleObject obj)
    {
        CurrentObject = obj;
    }

    public void DisableMovement()
    {
        Movement.enabled = false;
    }

    public void EnableMovement()
    {
        StartCoroutine(EnableMovementeWaiting());
    }

    bool picking = false;

    IEnumerator EnableMovementeWaiting()
    {
        while (picking) yield return null;
        Movement.enabled = true;
    }

    public bool dead = false;

    public bool Invulnerable = false;

    public void Death()
    {
        if (dead) return;
        dead = true;

        SoundManager.Instance.PlayCutSound(DeathSound.GetRandom(), transform);

        //Debug.Log("I AM DEAD");
        Movement.anim.SetTrigger("dead");
        Movement.enabled = false;
    }


    public void Restart()
    {

        StartCoroutine(DeadCoroutine());
    }

    IEnumerator DeadCoroutine()
    {
        yield return new WaitForSeconds(2);
        Fade.Instance.FadeO(EndGameAction);

    }

    void EndGameAction()
    {
        SceneManager.LoadScene("Environment");
    }

    private void Update()
    {
        if (!CurrentObject || !CurrentObject.isActiveAndEnabled) return;
        if (Input.GetKeyDown(KeyCode.Space) && CurrentObject)
        {
            CurrentObject.enabled = false;
            Character.Instance.DisableMovement();
            Movement.anim.SetBool("runFast", false);
            Movement.anim.SetBool("run", false);
            Movement.anim.SetTrigger("pick");
        }
    }

    public void OnPick()
    {
        if (!CurrentObject) return;
        picking = true;
        if (CurrentObject.Voice)
            CurrentObject.Voice.Play();
        GameManager.Instance.Interact(CurrentObject.Prefab.gameObject, CurrentObject.transform.position);
        Destroy(CurrentObject.gameObject);
        CurrentObject = null;
    }

    public void OnPickEnd()
    {
        picking = false;
    }
}
