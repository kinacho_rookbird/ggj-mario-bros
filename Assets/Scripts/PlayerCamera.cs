﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField] public Camera VRCamera;

    float cameraAngle;

    void Awake()
    {
        
            if (!VRCamera)
                VRCamera = GetComponentInChildren<Camera>();

            if (VRCamera)
                cameraAngle = VRCamera.fieldOfView;
        
    }
    void Update()
    {
        cameraAngle = VRCamera.fieldOfView;
    }

    public bool LookToPosition(Transform target)
    {
        Vector3 dir = target.position - VRCamera.transform.position;

        float angle = Mathf.Abs(Vector3.Angle(VRCamera.transform.forward, dir));

        return angle < cameraAngle*.8f;
    }

    public void EnablePostProcessing(bool b)
    {
        
    }
}
