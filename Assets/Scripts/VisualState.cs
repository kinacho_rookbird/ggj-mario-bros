﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualState : MonoBehaviour
{
    Material Material;
    
    [Header("Values")]
    [SerializeField] protected float Low;
    [SerializeField] protected float High;
  
    [SerializeField] float XTilingValue = 1;

    void Awake()
    {
        var Renderer = GetComponent<MeshRenderer>();
        Material = Renderer.sharedMaterial;
        Material.mainTextureOffset = new Vector2(XTilingValue, Low);
        Material = GetComponent<MeshRenderer>().material;
    }


    public virtual void SetValue(float v)
    {
        float current = v*(High-Low) + Low;

        if (v > 1)
            current = High;
        if (v < 0)
            current = Low;

        Material.mainTextureOffset=new Vector2(XTilingValue, current);
    }

}
