﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMannager : MonoBehaviour
{

    public GameObject fade;
    public GameObject exitMenu;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && exitMenu != null)
        {
            if (exitMenu.activeSelf)
            {
                exitMenu.SetActive(false);
                Time.timeScale = 1;
            }

            else
            {
                exitMenu.SetActive(true);
                Time.timeScale = 0;
            }
        }

}

    public void LoadScene(int scene)
    {
        StartCoroutine("changeScene",scene);
    }

    public void QuitGame()
    {
        StartCoroutine("quit");
    }

    IEnumerator changeScene(int id)
    {
        fade.SetActive(true);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(id);
    }

    IEnumerator quit()
    {
        fade.SetActive(true);
        yield return new WaitForSeconds(1);
        Application.Quit();
    }


}
