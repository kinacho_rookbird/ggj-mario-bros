﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractivePiece : MonoBehaviour
{

    public int index;

    private void OnMouseDown()
    {
        GameManager.Instance.FoundPiece();

        ImagePiece.Instance.EnableImage(index);

        InvulnerableZones.Instance.DestroyZone(index);
    }
}
