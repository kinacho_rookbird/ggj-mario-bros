﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteracibleObject : MonoBehaviour
{
    public Voice Voice;
    public ObjectMouseInteraction Prefab;

    private void OnTriggerEnter(Collider other)
    {
        var character = other.GetComponent<Character>();
        if (character)
        {
            character.SetObject(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var character = other.GetComponent<Character>();
        if (character)
        {
            character.SetObject(null);
        }
    }


}
