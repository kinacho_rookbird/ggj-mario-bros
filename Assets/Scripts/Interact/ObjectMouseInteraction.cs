﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMouseInteraction : MonoBehaviour
{
   
    private float speed = 5;

    private float pitch = 0.0f;
    private float yaw = 0.0f;
    private float moveX = 0.0f;
    private float moveY = 0.0f;

    bool pressed = false;
    bool secPressed = false;

    LocalObjectMouseInteraction LocalObject;

    Vector3 localPosition;
    Quaternion localRotation;

    Camera MainCamera;
    

    private void Start()
    {
        MainCamera = Camera.main;
        LocalObject = GetComponentInChildren<LocalObjectMouseInteraction>();
        if (LocalObject)
        {
            localPosition = LocalObject.transform.localPosition;
            localRotation = LocalObject.transform.localRotation;
        }
    }

    public void Restore()
    {
        if (LocalObject)
        {
            LocalObject.transform.localPosition = localPosition;
            LocalObject.transform.localRotation = localRotation;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            pressed = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            pressed = false;
            yaw = 0;
            pitch = 0;
        }

        if (Input.GetMouseButtonDown(1))
        {
            secPressed = true;
        }
        if (Input.GetMouseButtonUp(1))
        {
            secPressed = false;
            moveY = 0;
            moveX = 0;
        }

        if (pressed)
        {
            yaw = speed * Input.GetAxis("Mouse X");
            pitch = speed * Input.GetAxis("Mouse Y");

            Vector3 xAxis = new Vector3(0, 0, pitch);
            Vector3 yAxis = new Vector3(0, -yaw, 0);
            
            Quaternion rot = Quaternion.Euler(pitch, -yaw, 0) ;

            if (LocalObject)
            {
                LocalObject.transform.rotation = rot*LocalObject.transform.rotation;
                //LocalObject.transform.Rotate(xAxis, Space.World);
               // LocalObject.transform.Rotate(yAxis, Space.World);
            }
            else
            {
                transform.Rotate(xAxis, Space.World);
                transform.Rotate(yAxis, Space.World);
            }
            

        }

        if (secPressed)
        {
            Camera camera = MainCamera;

            if (camera)
            {
                if (LocalObject)
                {
                    LocalObject.transform.position +=
                  camera.transform.up * Input.GetAxisRaw("Mouse Y") * Time.deltaTime;

                    transform.position +=
                      camera.transform.right * Input.GetAxisRaw("Mouse X") * Time.deltaTime;
                }
                else
                {
                    LocalObject.transform.position +=
                  camera.transform.up * Input.GetAxisRaw("Mouse Y") * Time.deltaTime;

                    transform.position +=
                      camera.transform.right * Input.GetAxisRaw("Mouse X") * Time.deltaTime;
                }

            }
        }
    }
}
