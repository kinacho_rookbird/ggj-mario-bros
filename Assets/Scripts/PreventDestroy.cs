﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreventDestroy : MonoBehaviour
{

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

}
