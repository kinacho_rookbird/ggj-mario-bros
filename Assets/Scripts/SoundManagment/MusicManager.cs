﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{

    public static MusicManager Instance;

    [SerializeField] AudioClip EnemyMusic;
    [SerializeField] AudioClip EnviromentMusic;

    private void Start()
    {
        Instance = this;
        SoundManager.Instance.ChangeMusic(EnviromentMusic);
        //PlayEnviromentMusic();


    }

    bool enemy = false;

    public void PlayEnemyMusic()
    {
        if (enemy) return;

        enemy = true;
        SoundManager.Instance.ChangeMusic(EnemyMusic);
    }

    public void PlayEnviromentMusic()
    {
        if (!enemy) return;
        enemy = false;
        SoundManager.Instance.ChangeMusic(EnviromentMusic);
    }

}
