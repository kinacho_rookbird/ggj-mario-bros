﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceEventTrigger : MonoBehaviour
{

    [SerializeField] VoiceEvent Voice;

    private void OnTriggerEnter(Collider other)
    {
        var character = other.GetComponent<Character>();
        if (character)
        {
            //character.Movement.DisableMovement();
            //Voice.PlaySound(character.Movement.EnableMovement);
            Destroy(this.gameObject);
        }
    }

}
