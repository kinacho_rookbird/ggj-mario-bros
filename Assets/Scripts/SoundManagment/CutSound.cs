﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSound : MonoBehaviour
{

    [SerializeField] SoundData Sound;

    AudioSource source;

    public void Play()
    {
        source = SoundManager.Instance.PlayCutSound(Sound.GetRandom(), transform);
    }

    public void Stop()
    {
        if(source)
        source.Stop();
    }

}
