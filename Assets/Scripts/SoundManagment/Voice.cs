﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
struct Subtitle
{
    [SerializeField] public string subtitle;
    [SerializeField] public float time;
}

public class Voice : MonoBehaviour
{

    public static bool playing = false;

    [SerializeField] Subtitle[] Subtitles;
    [SerializeField] AudioClip VoiceClip;

    AudioSource CurrentSource;

    float timer = 0;

    int index = 0;

    bool play = false;
    bool init = false;

    public bool destroyGameObject = false;

    public void Play()
    {
        if (init) return;
        timer = 0;
        index = 0;
        play = true;
        init = true;
    }

    void OnVoiceFinish()
    {
        playing = false;
        SubtitleManager.Instance.SetSubtitle("");
        if (!destroyGameObject)
            Destroy(this);
        else
            Destroy(gameObject);
    }

    private void Update()
    {
        if (!init) return;
        if (play)
        {
            if (!playing)
            {
                play = false;
                playing = true;
                SoundManager.Instance.PlayVoice(VoiceClip, 0, OnVoiceFinish);
            }
            else return;
        }

        if (!play)
        {
            timer += Time.deltaTime;

            if (index < Subtitles.Length)
            {
                Subtitle sub = Subtitles[index];
                if (timer > sub.time)
                {
                    string subtitle = Subtitles[index].subtitle;
                    var split = subtitle.Split('\\');
                    subtitle = "";
                    for(int i=0; i < split.Length; i++)
                    {
                        subtitle += split[i] + "\n";
                    }
                    SubtitleManager.Instance.SetSubtitle(subtitle);
                    index++;
                }
            }
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        var character = other.GetComponent<Character>();
        if (character)
        {
            Play();
        }
    }

} 
