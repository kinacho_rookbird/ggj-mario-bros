﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour
{
    AudioSource AudioSource;

    [SerializeField] SoundData Data;

    void Awake()
    {
        AudioSource = GetComponent<AudioSource>();
        AudioSource.playOnAwake = false;
    }

    public void PlayRandomSound()
    {
        AudioClip clip = Data.GetRandom();

        AudioSource.clip = clip;
        AudioSource.Play();
        //StartCoroutine(Play());
    }

}
