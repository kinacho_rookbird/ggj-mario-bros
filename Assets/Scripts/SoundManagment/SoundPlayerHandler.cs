﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum SoundPlayerTag { Hitting, Handle, Handling, Falling, OnStump, WoodSpiltting }

[System.Serializable]
public struct SoundPlayerClassify
{
    [SerializeField] public SoundPlayerTag Tag;
    [SerializeField] public SoundPlayer Player;
}

public class SoundPlayerHandler : MonoBehaviour
{
    [SerializeField] SoundPlayerClassify[] SoundPlayers;

    private Dictionary<SoundPlayerTag, SoundPlayer> _dictionary;

    private void Awake()
    {
        _dictionary = new Dictionary<SoundPlayerTag, SoundPlayer>();
        foreach(var pc in SoundPlayers)
        {
            _dictionary.Add(pc.Tag, pc.Player);
        }
    }

    public void PlayRandomSound(SoundPlayerTag tag)
    {
        if (_dictionary.ContainsKey(tag))
        {
            _dictionary[tag].PlayRandomSound();
        }
    }

}
