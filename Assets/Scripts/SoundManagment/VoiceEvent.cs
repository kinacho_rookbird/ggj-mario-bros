﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceEvent : MonoBehaviour
{
    [SerializeField] SoundData Voice;

    bool played = false;

    public void PlaySound(Action callback)
    {
        if (!played)
            SoundManager.Instance.PlayVoice(Voice.GetRandom(), 0, callback);
        played = true;
    }
}
