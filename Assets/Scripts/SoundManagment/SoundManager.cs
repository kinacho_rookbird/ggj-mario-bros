﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SoundManager : MonoBehaviour
{
    private static SoundManager _instance;
    public static SoundManager Instance 
    { 
        get 
        {
            if (!_instance) _instance = FindObjectOfType<SoundManager>();
            
            return _instance; 
        } 
    }

    public float Volume;

    [SerializeField] AudioSource[] AudioSources;
    [SerializeField] AudioSource[] Music;

    int musicIndex = 0;

    int _index = 0;

    [SerializeField] AudioSource CutAudioSources;

    int _indexcut = 0;

    [SerializeField] AudioSource Voices;

    Queue<AudioClip> voiceclips;

    private void Awake()
    {
        _instance = this;
        voiceclips = new Queue<AudioClip>();
    }

    public void StopAllSounds(string tag)
    {
        foreach(var a in AudioSources)
        {
            if (a.tag == tag)
                a.Stop();
        }
    }

    public AudioSource PlayEffect(AudioClip clip, Transform emmiter, float pitch_value)
    {
        AudioSource audioSource = AudioSources[_index];

        audioSource.Stop();
        audioSource.clip = clip;
        audioSource.transform.position = emmiter.position;
        audioSource.tag = emmiter.tag;
        audioSource.pitch = pitch_value;
        audioSource.Play();

        _index = (_index + 1) % AudioSources.Length;

        return audioSource;
    }

    internal AudioSource PlayCutSound(AudioClip clip, Transform tr)
    {
        AudioSource audioSource = Instantiate<AudioSource>(CutAudioSources);

        //audioSource.Stop();
        audioSource.transform.position = tr.position;
        audioSource.clip = clip;
        audioSource.gameObject.SetActive(true);
        audioSource.Play();
        
        return audioSource;
    }

    public AudioSource PlayEffect(AudioClip clip, Transform transform)
    {
        return PlayEffect(clip, transform, 1);
    }

    internal void ChangeEffectsVolume(float value)
    {
        Volume = value;
        foreach (var a in AudioSources)
        {
            a.volume = value;
        }

    }

    public void PlayVoice(AudioClip clip, float silencio, Action callback)
    {
        //voiceclips.Enqueue(clip);
        Voices.clip = clip;

        Voices.Play();
        Music[musicIndex].volume = 0.5f;
        StartCoroutine(WaitForFinishVoice(Voices, silencio, callback));
    }

    IEnumerator WaitForFinishVoice(AudioSource source, float silencio, Action callback)
    {
        yield return new WaitForSecondsRealtime(silencio);
        while (source.isPlaying)
            yield return new WaitForSecondsRealtime(1f);
        Music[musicIndex].volume = 1;
        callback?.Invoke();

    }


    bool changeMusic = false;

    private void Update()
    {
        if (voiceclips.Count > 0 && !Voices.isPlaying)
        {
            var clip = voiceclips.Dequeue();

            Voices.clip = clip;

            Voices.Play();

        }

        if (changeMusic)
        {
            int prev = (musicIndex - 1 + Music.Length) % Music.Length;

            if (Music[musicIndex].volume < 1)
            {
                Music[musicIndex].volume += Time.deltaTime;
                Music[prev].volume -= Time.deltaTime;
            }
            else
            {
                Music[prev].Stop();
                changeMusic = false;
            }
        }
    }

    public void ChangeMusic(AudioClip clip)
    {
        changeMusic = true;
        musicIndex = (musicIndex + 1) % Music.Length;
        Music[musicIndex].clip = clip;
        Music[musicIndex].Play();

    }



}
