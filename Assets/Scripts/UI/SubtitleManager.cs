﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubtitleManager : MonoBehaviour
{

    public static SubtitleManager Instance;

    [SerializeField] Text Text;

    private void Awake()
    {
        Instance = this;
        Text.text = "";
    }

    public void SetSubtitle(string text)
    {
        Text.text = text;
    }

}
