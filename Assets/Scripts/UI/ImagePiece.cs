﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImagePiece : MonoBehaviour
{
    public static ImagePiece Instance;

    [SerializeField] Image[] Images;

    private void Awake()
    {
        Instance = this;
    }


    public void EnableImage(int index)
    {
        if (index < Images.Length)
        {
            Images[index].gameObject.SetActive(true);
        }
    }

}
