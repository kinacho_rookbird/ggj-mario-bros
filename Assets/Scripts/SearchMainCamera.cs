﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchMainCamera : MonoBehaviour
{
    [SerializeField] Canvas Canvas;
    // Start is called before the first frame update
    void Awake()
    {
        Canvas = GetComponent<Canvas>();
        Canvas.worldCamera = Camera.main;


    }

}
