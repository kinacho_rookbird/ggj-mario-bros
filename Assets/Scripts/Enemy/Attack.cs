﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{

    Enemy enemy;

    private void Awake()
    {
        enemy = GetComponentInParent<Enemy>();
    }

    public void AttackFinished()
    {
        enemy.AttackFinished();
    }

    public void OnAttack()
    {
       // Debug.Log("On Attack");
        enemy.OnAttack();
    }
}
