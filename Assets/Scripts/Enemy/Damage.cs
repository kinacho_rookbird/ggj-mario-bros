﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    public Character Character;

    float timer = 0;

    bool exit = false;
    private void OnTriggerEnter(Collider other)
    {
        var character = other.GetComponent<Character>();
        if (character)
        {
            //Debug.LogError("Damage enter");
            timer = 0;
            Character = character;
            exit = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var character = other.GetComponent<Character>();
        if (character)
        {
            //Debug.LogError("Damage exit");
            exit = true;
    //        Character = null;
        }
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer > 1)
        {
            if (exit)
            {
                Character = null;
            }
        }

    }


}
