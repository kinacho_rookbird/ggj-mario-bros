﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public enum EnemyState
{
    None,
    Idle,
    Moving,
    Attack
}

public class Enemy : MonoBehaviour
{
    public GameObject player;
    public float walkSpeed;
    public float runSpeed;
    public NavMeshAgent navMesh;
    public float chaseDistance;
    public float attackDistance = .77f;
    public float maxDistance = 20;

    public Animator anim;
    [SerializeField] private bool chasingPlayer;

    [SerializeField] EnemyState State = EnemyState.Idle;
    EnemyState Prev = EnemyState.Idle;

    [SerializeField] SoundData Grito;
    [SerializeField] SoundData Susurro;
    [SerializeField] SoundData swoosh;
    [SerializeField] SoundData AttackSound;


    [SerializeField] float aimthreshold = .3f;

    PlayerSteps Steps;

    Damage Damage;


    [SerializeField] Transform[] RandomTransforms;

    public string status;

    public void ChangeState(EnemyState state)
    {
        Prev = State;
        State = state;
    }

    // Start is called before the first frame update
    void Start()
    {
        Damage = GetComponentInChildren<Damage>();
        Steps = GetComponentInChildren<PlayerSteps>();
        navMesh.speed = walkSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        switch (State) 
        {
            case EnemyState.Idle:
                Idle(Time.deltaTime);
                break;
            case EnemyState.Moving:
                Move(Time.deltaTime);
                break;
            case EnemyState.Attack:
                Attack(Time.deltaTime);
                break;
            case EnemyState.None:
                None();
                break;
        }
            
    }

    internal void WakeUp()
    {
        Vector3 direction = new Vector3(Random.Range(0, 1f), 0, Random.Range(0, 1f));
        direction.Normalize();
        direction *= 20;

        SoundManager.Instance.PlayEffect(Grito.GetRandom(), transform);
        ChangeState(EnemyState.Moving);
    }

    internal void PrevState()
    {
        State = Prev;
    }

    private void None()
    {
        navMesh.speed = 0;
    }

    float timer = 0;
    float time = 25;

    bool chase = false;

    private void Move(float deltaTime)
    {
        if (!navMesh.enabled) return;
        navMesh.speed = walkSpeed;
        if (Vector3.Distance(player.transform.position, transform.position) < maxDistance
            && !Character.Instance.Invulnerable && GameManager.Instance.enemyEnabled
            )
        {
            navMesh.SetDestination(player.transform.position);
            status = "buscando player";
            chase = true;
        }
        else
        {
            {
                if (Character.Instance.Invulnerable)
                {
                    status = "random";

                    navMesh.SetDestination(
                        RandomTransforms[UnityEngine.Random.Range(0, RandomTransforms.Length - 1)].position);
                }
                else
                {
                    status = "buscando cerca de player";

                    navMesh.SetDestination(player.transform.position + new Vector3(UnityEngine.Random.Range(0, 1f), player.transform.position.y, UnityEngine.Random.Range(0, 1f)) * UnityEngine.Random.Range(0, 20f));

                }
            }
            chase = false;
        }

        if (navMesh.velocity.magnitude >= 0.1f)
            anim.SetBool("walk", true);
        else if (navMesh.velocity.magnitude == 0)
            anim.SetBool("walk", false);

        if (navMesh.remainingDistance < attackDistance && chase && !Character.Instance.dead)
        {
            if(PlayerOnVisual())
                ChangeState(EnemyState.Attack);
        }

        if (!PlayerOnVisual() && chase)
        {
            Vector3 dir = (player.transform.position - transform.position);
            dir.Normalize();
            Vector3 pos = player.transform.position + dir * 3;
            navMesh.SetDestination(pos);
        }

        if (navMesh.remainingDistance < chaseDistance && chase)
        {
            anim.SetBool("run", true);
            navMesh.speed = runSpeed;
        }
        else
        {
            Steps.ResetSound();
            navMesh.speed = walkSpeed;
            anim.SetBool("run", false);
        }

        timer += deltaTime;
        if (timer > time)
        {
            time = 10 + UnityEngine.Random.Range(0, 5f);
            timer = 0;
            SoundManager.Instance.PlayCutSound(Susurro.GetRandom(), Character.Instance.transform);

        }

    }

    bool att = false;

    private void Attack(float deltaTime)
    {
        Damage.gameObject.SetActive(true);
        if (!att)
        {
            anim.SetTrigger("attack");
            att = true;
            //SoundManager.Instance.PlayCutSound(Susurro.GetRandom(), transform);
        }

        navMesh.speed = 0;
        //anim.SetBool("walk", false);
        //anim.SetBool("run", false);
        

    }

    private void Idle(float deltaTime)
    {
        status = "idle";

        navMesh.speed = 0;
        anim.SetBool("walk", false);
        anim.SetBool("run", false);
        anim.SetBool("idle", true);

    }

    internal void AttackFinished()
    {
       
        att = false;
        ChangeState(EnemyState.Moving);
        Damage.gameObject.SetActive(false);
    }

    public void OnAttack()
    {
        SoundManager.Instance.PlayEffect(swoosh.GetRandom(), transform);

        if (Damage.Character)
        {
            SoundManager.Instance.PlayEffect(AttackSound.GetRandom(), transform);
            Damage.Character.Death();
        }
    }


    bool PlayerOnVisual()
    {
        Vector3 forward = transform.forward;
        Vector3 pos = transform.position;
        Vector3 playerPos = player.transform.position;

        Vector3 aim = playerPos - pos;

        aim.Normalize();

        float dot = Vector3.Dot(aim, forward);

        return dot > 1-aimthreshold;



    }


    public void Spawn(Vector3 pos)
    {
        navMesh.enabled = false;
        transform.position = pos;
        StartCoroutine(Coroutine());
    }

    IEnumerator Coroutine()
    {
        yield return new WaitForSeconds(1);
        navMesh.enabled = true;
    }

}
