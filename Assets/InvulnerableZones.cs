﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvulnerableZones : MonoBehaviour
{

    public static InvulnerableZones Instance;

    [SerializeField] InvulnerablePlayerTrigger[] Triggers;

    private void Awake()
    {
        Instance = this;
    }

    public void DestroyZone(int index)
    {
        if (index < Triggers.Length)
        {
            Destroy(Triggers[index].gameObject);
        }
        else Debug.LogError("Index out InvulnerableZones");
    }


}
