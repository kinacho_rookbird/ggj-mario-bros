﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPlaces : MonoBehaviour
{
    [SerializeField] Transform[] Transforms;

    Enemy Enemy;

    private void Awake()
    {
        if(!Enemy)
        Enemy = FindObjectOfType<Enemy>();
    }

    public void SpawnEnemy(Vector3 position)
    {

        Transform nearest = Transforms[0];
        foreach(var tr in Transforms)
        {
            if(Vector3.Distance(tr.position,position) < Vector3.Distance(nearest.position, position))
            {
                nearest = tr;
            }
        }

        Enemy.Spawn(nearest.position);

    }

}
