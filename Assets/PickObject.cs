﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickObject : MonoBehaviour
{

    [SerializeField] SoundData PickSound;

    public void Pick()
    {
        SoundManager.Instance.PlayEffect(PickSound.GetRandom(), transform);
        Character.Instance.OnPick();
    }

    public void PickEnd()
    {
        Character.Instance.OnPickEnd();

    }
}
