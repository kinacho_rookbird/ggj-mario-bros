﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance;

    public int ObjCount = 0;
    [SerializeField] int MaxObjCount = 8;

    [SerializeField] Transform InteractivePosition;

    [SerializeField] Camera OverlayCamera;

    [SerializeField] Enemy Enemy;
    [SerializeField] Character Character;

    [SerializeField] float Probability = 0;

    [SerializeField] GameObject LastObject;

    [SerializeField] Voice LastVoice;
    [SerializeField] AudioClip Chastquido;
    [SerializeField] RandomPlaces RandomPlaces;
    void Awake()
    {
        Instance = this;
        
    }

    private void Start()
    {
        //Enemy.WakeUp();
    }

    GameObject Object;
    Vector3 SpawnPos;
    public void Interact(GameObject prefab, Vector3 sp)
    {
        SpawnPos = sp;
        Object = Instantiate(prefab, InteractivePosition.transform.position, InteractivePosition.transform.rotation, OverlayCamera.transform);
        Enemy.ChangeState(EnemyState.None);
    }

    public bool enemyEnabled = false;

    public void FoundPiece()
    {
        if (Object) Destroy(Object);

        Character.Instance.EnableMovement();
        Enemy.PrevState();

        ObjCount++;

        Probability += ObjCount * 0.1f;

        float r = Random.Range(0, 1f);

        if (r < Probability)
        {
            RandomPlaces.SpawnEnemy(SpawnPos);
            if (!enemyEnabled)
                Enemy.WakeUp();
            enemyEnabled = true;

            //MusicManager.Instance.PlayEnemyMusic();
        }

        if (ObjCount >= MaxObjCount - 1)
        {
            if(LastObject)
                LastObject.SetActive(true);
        }

        if(ObjCount >= MaxObjCount)
        {
            EndGame();
        }

    }

    public void EndGame()
    {
        StartCoroutine(EndGameCoroutine());
    }

    IEnumerator EndGameCoroutine()
    {
        yield return new WaitForSeconds(7);
        LastVoice.Play();

        while (LastVoice) yield return null;

        Character.Instance.DisableMovement();
        Enemy.gameObject.SetActive(false);

        Fade.Instance.FadeO(EndGameAction);
    }

    public void EndGameAction()
    {
        SoundManager.Instance.PlayEffect(Chastquido, Character.Instance.transform);
        StartCoroutine(EndGameCoroutine2());
    }

    IEnumerator EndGameCoroutine2()
    {
        yield return new WaitForSeconds(2);

        UnityEngine.SceneManagement.SceneManager.LoadScene("Credits");

    }

}
