﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
    public static Fade Instance;

    [SerializeField] GameObject FadeOut;

    private void Awake()
    {
        Instance = this;
    }

    Action Action;

    public void FadeO(Action action)
    {
        Action = action;
        FadeOut.SetActive(true);
        StartCoroutine(Coroutine());
    }

    IEnumerator Coroutine()
    {
        yield return new WaitForSeconds(3);
        Action?.Invoke();

    }

}
